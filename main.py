from flask import Flask, request, redirect, render_template
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import re

app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://build-a-blog:launchcode@localhost:8889/build-a-blog'
app.config['SQLALCHEMY_ECHO'] = True
db = SQLAlchemy(app)


class Blog(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120))
    body = db.Column(db.String(1000))
    pub_date = db.Column(db.DateTime)
    image = db.Column(db.String(1000))

    def __init__(self, title, body, image_url=None, pub_date=None):
        self.title = title
        self.body = body
        self.image = image_url
        #http://flask-sqlalchemy.pocoo.org/2.1/quickstart/#simple-relationships
        if pub_date is None:
            pub_date = datetime.utcnow()
        self.pub_date = pub_date


@app.route('/blog')
def index():
    current_blog_id = request.args.get('id')
    #if GET request from clicking on blog entry's title, then go to that blog's page
    if current_blog_id is not None:
        blog = Blog.query.get(current_blog_id)

        #Find the blog posts right before and after this one by id
        #https://stackoverflow.com/questions/24597932/sqlalchemy-previous-row-and-next-row-by-id
        prev_blog = Blog.query.order_by(Blog.id.desc()).filter(Blog.id < blog.id).first()
        next_blog = Blog.query.order_by(Blog.id.asc()).filter(Blog.id > blog.id).first()

        return render_template('postpage.html', blog=blog, prev_blog=prev_blog, next_blog=next_blog)

    #else it's a GET request to display main page
    blogs = Blog.query.order_by(Blog.pub_date.desc()).all()
    return render_template('index.html',
        blogs=blogs)


@app.route('/new-post', methods=['POST', 'GET'])
def newpost():
    #new blog post submitted
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        image = request.form['image']
        title_error = ''
        body_error = ''

        if title == '':
            title_error = "Please fill in the title"
        if body == '':
            body_error = "Please fill in the body"

        #submission is valid
        if not (title_error or body_error):
            new_blog = Blog(title, body, image)
            db.session.add(new_blog)
            db.session.commit()
            blog_id = str(new_blog.id)
            return redirect('/blog?id=' + blog_id)

        return render_template('newpost.html',
            title=title,
            body=body,
            image=image,
            title_error=title_error,
            body_error=body_error)
    #GET request from clicking on "add a new post" link
    return render_template('newpost.html')


@app.route('/delete-post', methods=['POST'])
def delete_post():
    blog_id = int(request.form['blog_id'])
    blog = Blog.query.get(blog_id)
    db.session.delete(blog)
    db.session.commit()
    return redirect('/blog')


@app.route('/edit-post', methods=['POST', 'GET'])
def edit_post():
    #updated blog post submitted
    if request.method == 'POST':
        blog_id = int(request.form['blog_id'])
        title = request.form['title']
        body = request.form['body']
        image = request.form['image']
        title_error = ''
        body_error = ''

        if title == '':
            title_error = "Please fill in the title"
        if body == '':
            body_error = "Please fill in the body"

        #submission is valid, update blog entry
        if not (title_error or body_error):
            blog = Blog.query.get(blog_id)
            blog.title = title
            blog.body = body
            blog.image = image
            db.session.add(blog)
            db.session.commit()
            return redirect('/blog?id=' + str(blog_id))

        return render_template('editpost.html',
            title=title,
            body=body,
            image=image,
            blog_id=blog_id,
            title_error=title_error,
            body_error=body_error)

    #edit button clicked
    blog_id = request.args.get('blog_id')
    blog = Blog.query.get(blog_id)
    return render_template('editpost.html',
        title=blog.title,
        body=blog.body,
        image=blog.image,
        blog_id=blog.id)


def split_semicolon(string):
    """ custom filter to split string by ';'. Also removes whitespace and strips trailing ';' """
    #https://stackoverflow.com/questions/20678004/jinja2-split-string-by-white-spaces
    #https://stackoverflow.com/questions/4071396/split-by-comma-and-strip-whitespace-in-python
    return re.sub(r'\s', '', string).rstrip(";").split(';')

if __name__ == '__main__':
    app.jinja_env.filters['split_semicolon'] = split_semicolon
    app.run()
